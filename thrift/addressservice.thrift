namespace java com.krowd9.address.server.domain

include "common.thrift"
include "addressbook.thrift"

struct Contact {
  1: optional string name;
  2: optional common.UserId userId;
  3: required common.ExternalUserId externalId;
  4: required addressbook.Offset offset;
}

struct GetContactsResult {
  1: list<Contact> contacts
  2: required bool hasMore
  3: required addressbook.GetContactsFilter filter
}

service AddressBookService {

   GetContactsResult getContacts(1: common.UserId userId, 2: addressbook.GetContactsFilter filter)

}