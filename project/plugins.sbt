resolvers += Resolver.bintrayRepo("twittercsl", "sbt-plugins")

addSbtPlugin("com.twitter" % "scrooge-sbt-plugin" % "18.7.0")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")