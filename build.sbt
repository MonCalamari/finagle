import com.twitter.scrooge.ScroogeSBT

name := "finagle"

version := "0.1"

scalaVersion := "2.12.6"

lazy val root = project
  .in(file("."))
  .settings(libraryDependencies ++= Seq(
    "org.apache.thrift" % "libthrift"       % "0.11.0",
    "com.twitter"       %% "scrooge-core"   % "18.7.0" exclude ("com.twitter", "libthrift"),
    "com.twitter"       %% "finagle-thrift" % "18.7.0" exclude ("com.twitter", "libthrift"),
    "org.scalatest"     %% "scalatest"      % "3.0.1" % Test,
    "org.scalacheck"    %% "scalacheck"     % "1.13.5" % Test,
    "org.easymock"      % "easymock"        % "3.4" % Test
  ))
  .settings(scroogeThriftSourceFolder in Compile := { baseDirectory.value / "thrift" })
  .settings(mainClass in run := Some("com.krowd9.server.AddressBookServer"))
  .enablePlugins(ScroogeSBT)