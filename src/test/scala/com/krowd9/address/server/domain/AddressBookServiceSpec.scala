package com.krowd9.address.server.domain

import com.krowd9.api.addressbookdb.{AddressBookContact, AddressBookDbService, GetContactsFilter, OffsetAndContact}
import com.krowd9.api.usermanager.{GetYakatakUserIdResult, UserManagerService}
import com.twitter.util.Future
import org.scalatest.concurrent.{IntegrationPatience, ScalaFutures}
import org.scalatest.easymock.EasyMockSugar
import org.scalatest.{Matchers, Outcome, fixture}

class AddressBookServiceSpec extends fixture.WordSpec with Matchers with EasyMockSugar with ScalaFutures with IntegrationPatience {

  "AddressBookService" should {

    "not return any contacts if address book is empty" in { f =>
      import f._

      val userId = 5
      val filter = GetContactsFilter()

      expecting {
        addressBookEndpointMock.getContacts(userId, filter)
        lastCall.andReturn(Future.value(Seq.empty))
        lastCall.once()
      }

      whenExecuting(addressBookEndpointMock) {
        val result = underTest.getContacts(userId, filter).asScalaFuture.futureValue
        result.contacts should be(empty)
        result.hasMore shouldBe false
        result.filter.offset shouldBe None
      }

    }

    "not return any contacts if address book API returns poison pill only" in { f =>
      import f._

      val userId = 3
      val filter = GetContactsFilter()

      expecting {
        addressBookEndpointMock.getContacts(userId, filter)
        lastCall.andReturn(Future.value(Seq(poisonPill)))
        lastCall.once()
      }

      whenExecuting(addressBookEndpointMock, userManagerEndpoint) {
        val result = underTest.getContacts(userId, filter).asScalaFuture.futureValue
        result.contacts should be(empty)
        result.hasMore shouldBe false
        result.filter.offset shouldBe None
      }
    }

    "return single contact with yakatak id" in { f =>
      import f._

      val userId    = 6
      val yakatakId = Some(7L)
      val filter    = GetContactsFilter()

      expecting {
        addressBookEndpointMock.getContacts(userId, filter)
        lastCall.andReturn(Future.value(Seq(OffsetAndContact("offset", AddressBookContact("externalId", None)))))
        addressBookEndpointMock.getContacts(userId, GetContactsFilter(Some("offset")))
        lastCall.andReturn(Future.value(Seq(poisonPill)))
        userManagerEndpoint.getYakatakUserId("externalId")
        lastCall.andReturn(Future.value(GetYakatakUserIdResult(yakatakId)))
      }

      whenExecuting(addressBookEndpointMock, userManagerEndpoint) {
        val result = underTest.getContacts(userId, filter).asScalaFuture.futureValue
        result.contacts should not be empty
        result.contacts.headOption shouldBe Some(Contact(None, yakatakId, "externalId", "offset"))
        result.hasMore shouldBe false
        result.filter.offset shouldBe Some("offset")
      }
    }

    "start getting address book contacts from an offset" in { f =>
      import f._

      val userId = 9
      val filter = GetContactsFilter(Some("offset"))

      expecting {
        addressBookEndpointMock.getContacts(userId, filter)
        lastCall.andReturn(Future.value(Seq(OffsetAndContact("offset2", AddressBookContact("externalId")))))
        lastCall.once()
        addressBookEndpointMock.getContacts(userId, GetContactsFilter(Some("offset2")))
        lastCall.andReturn(Future.value(Seq(poisonPill)))
        userManagerEndpoint.getYakatakUserId("externalId")
        lastCall.andReturn(Future.value(GetYakatakUserIdResult()))
      }

      whenExecuting(addressBookEndpointMock, userManagerEndpoint) {
        val result = underTest.getContacts(userId, filter).asScalaFuture.futureValue
        result.contacts should contain theSameElementsAs Seq(Contact(None, None, "externalId", "offset2"))
        result.hasMore shouldBe false
        result.filter.offset shouldBe Some("offset2")
      }
    }

  }

  override protected def withFixture(test: OneArgTest): Outcome = {
    super.withFixture(test.toNoArgTest(FixtureParam()))
  }

  case class FixtureParam() {

    val poisonPill = OffsetAndContact("offset", AddressBookContact("ext!63"))

    val addressBookEndpointMock: AddressBookDbService.MethodPerEndpoint = mock[AddressBookDbService.MethodPerEndpoint]
    val userManagerEndpoint:     UserManagerService.MethodPerEndpoint   = mock[UserManagerService.MethodPerEndpoint]

    val underTest: AddressBookService[Future] = AsyncAddressBookService(addressBookEndpointMock, userManagerEndpoint)
  }

}
