package com.krowd9.address.server

import com.twitter.util.{Return, Throw, Future => TwitterFuture}

import scala.concurrent.{Future, Promise}

package object domain {

  implicit class RichTwitterFuture[A](twitterFuture: TwitterFuture[A]) {

    def asScalaFuture: Future[A] = {
      val promise = Promise[A]()
      twitterFuture respond {
        case Return(a) => promise success a
        case Throw(e)  => promise failure e
      }
      promise.future
    }

  }

}
