package com.krowd9.address.server.domain

import com.krowd9.api.addressbookdb.{AddressBookDbService, GetContactsFilter}
import com.krowd9.api.usermanager.UserManagerService
import com.twitter.finagle.stats.StatsReceiver
import com.twitter.util.Future

private[krowd9] trait AsyncAddressBookService extends AddressBookService[Future] with Contacts with Offsets {

  def contactMaxFetchSize: Int = 100

  override def getContacts(userId: Long, filter: GetContactsFilter): Future[GetContactsResult] = {
    for {
      offsets <- fetchOffsets(userId, filter, contactMaxFetchSize)
      hasMore  = hasMoreOffsets(offsets)
      elements = if (hasMore) offsets else offsets.dropRight(1)
      contacts <- Future.collect(elements.map(fetchContactForOffset))
      lastOffset = contacts.lastOption.map(_.offset)
    } yield GetContactsResult(contacts, hasMore, GetContactsFilter(lastOffset))
  }

}

private[krowd9] trait AddressBookServiceMetrics extends AsyncAddressBookService {

  def statsReceiver: StatsReceiver

  private lazy val contactsCounter = statsReceiver.counter("get-contacts-counter")

  private lazy val failedContactsCounter = statsReceiver.counter("get-contacts-failed-counter")

  override def getContacts(userId: Long, filter: GetContactsFilter): Future[GetContactsResult] = {
    contactsCounter.incr()
    super.getContacts(userId, filter).onFailure(_ => failedContactsCounter.incr())
  }

}

private[krowd9] object AsyncAddressBookService {

  def apply(addressBookServiceEndpoint: AddressBookDbService.MethodPerEndpoint, userManagerServiceEndpoint: UserManagerService.MethodPerEndpoint): AddressBookService[Future] =
    new AsyncAddressBookService {

      override val addressBookEndpoint: AddressBookDbService.MethodPerEndpoint = addressBookServiceEndpoint

      override val userManagerEndpoint: UserManagerService.MethodPerEndpoint = userManagerServiceEndpoint
    }

  def apply(
    addressBookServiceEndpoint: AddressBookDbService.MethodPerEndpoint,
    userManagerServiceEndpoint: UserManagerService.MethodPerEndpoint,
    stats:                      StatsReceiver): AddressBookService[Future] = new AddressBookServiceMetrics {

    override val addressBookEndpoint: AddressBookDbService.MethodPerEndpoint = addressBookServiceEndpoint

    override val userManagerEndpoint: UserManagerService.MethodPerEndpoint = userManagerServiceEndpoint

    override val statsReceiver: StatsReceiver = stats
  }

}
