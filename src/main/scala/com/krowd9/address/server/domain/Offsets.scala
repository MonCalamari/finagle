package com.krowd9.address.server.domain

import com.krowd9.api.addressbookdb.{AddressBookDbService, GetContactsFilter, OffsetAndContact}
import com.krowd9.api.common.UserNotFoundException
import com.twitter.util.Future

private[krowd9] trait Offsets {

  private val poisonPillId = "ext!63"

  def addressBookEndpoint: AddressBookDbService.MethodPerEndpoint

  /**
    * The method below validates if the fetching algorithm can continue fetching contacts.
    * The API seems to return `Éva Tardos` with `ext!63` external id to signal end of address book (poison pill).
    */
  def hasMoreOffsets(offsets: Seq[OffsetAndContact]): Boolean = {
    offsets.nonEmpty && !(offsets.last.contact.externalId == poisonPillId)
  }

  def fetchOffsets(userId: Long, filter: GetContactsFilter, limit: Int): Future[Seq[OffsetAndContact]] = {

    def fetch(state: Seq[OffsetAndContact], filter: GetContactsFilter): Future[Seq[OffsetAndContact]] = {
      if (state.size >= limit) {
        Future.value(state)
      } else {
        // @formatter:off
        for {
          contacts <- addressBookEndpoint.getContacts(userId, filter).rescue { case _: UserNotFoundException => Future.value(Seq.empty) }
          newState =  state ++ contacts
          result   <- if (hasMoreOffsets(contacts)) fetch(newState, GetContactsFilter(Some(contacts.last.offset))) else Future.value(newState)
        } yield result
        // @formatter:on
      }
    }

    fetch(Seq.empty, filter)
  }

}
