package com.krowd9.address.server.domain

import com.github.benmanes.caffeine.cache.{CacheLoader, Caffeine, LoadingCache}
import com.krowd9.api.addressbookdb.OffsetAndContact
import com.krowd9.api.usermanager.{GetYakatakUserIdResult, UserManagerService}
import com.twitter.cache.caffeine.LoadingFutureCache
import com.twitter.util.Future

private[krowd9] trait Users {

  def userManagerEndpoint: UserManagerService.MethodPerEndpoint

  def getYakatakUserId(externalId: String): Future[GetYakatakUserIdResult] = userManagerEndpoint.getYakatakUserId(externalId)

}

private[krowd9] trait CachedUsers extends Users {

  private val loader: CacheLoader[String, Future[GetYakatakUserIdResult]] = (externalId: String) => super.getYakatakUserId(externalId)

  private val caffeine: LoadingCache[String, Future[GetYakatakUserIdResult]] =
    Caffeine.newBuilder().build(loader)

  private val futureCache: LoadingFutureCache[String, GetYakatakUserIdResult] = new LoadingFutureCache(caffeine)

  override def getYakatakUserId(externalId: String): Future[GetYakatakUserIdResult] = futureCache(externalId)

}

private[krowd9] trait Contacts extends CachedUsers {

  def fetchContactForOffset(offset: OffsetAndContact): Future[Contact] = {
    val name       = offset.contact.name
    val externalId = offset.contact.externalId
    getYakatakUserId(externalId).map { result =>
      Contact(name, result.userId, externalId, offset.offset)
    }
  }

}
