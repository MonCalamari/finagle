package com.krowd9.address.server

import com.krowd9.address.client.{AddressBookDbClient, UserManagerClient}
import com.krowd9.address.server.domain.{AddressBookService, AsyncAddressBookService}
import com.twitter.finagle.{ListeningServer, Thrift}
import com.twitter.server.TwitterServer
import com.twitter.util.{Await, Future}

private[krowd9] object AddressBookServer extends TwitterServer {

  def main(): Unit = {

    val addressBookService: AddressBookService[Future] = AsyncAddressBookService(
      AddressBookDbClient(":7201"),
      UserManagerClient(":7202"),
      statsReceiver
    )

    val server: ListeningServer = Thrift.server.serveIface(":7203", addressBookService)

    onExit {
      server.close()
    }

    Await.ready(adminHttpServer)

  }

}
