package com.krowd9.address.client

import com.krowd9.api.usermanager.UserManagerService
import com.twitter.finagle.Thrift

private[krowd9] object UserManagerClient {

  def apply(endpoint: String): UserManagerService.MethodPerEndpoint = {
    Thrift.client.build[UserManagerService.MethodPerEndpoint](endpoint)
  }

}
