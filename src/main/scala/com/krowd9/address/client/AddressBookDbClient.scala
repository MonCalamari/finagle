package com.krowd9.address.client

import com.krowd9.api.addressbookdb.AddressBookDbService
import com.twitter.finagle.Thrift

private[krowd9] object AddressBookDbClient {

  def apply(endpoint: String): AddressBookDbService.MethodPerEndpoint = {
    Thrift.client.build[AddressBookDbService.MethodPerEndpoint](endpoint)
  }

}
